package Main;

import Presentation.View;
import Presentation.Controller;

public class MainClass {

	public static void main(String[] args) throws Exception {
		
		View view = new View();
		new Controller(view);
	}
}
