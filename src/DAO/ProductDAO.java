package DAO;

import DBAcces.ConnectionFactory;
import Model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ProductDAO {

	private static final String findProductById = "SELECT * FROM product WHERE productId=?";
	private static final String insertProductQuery = "INSERT INTO product (productId,productName,price,quantity) VALUES (?,?,?,?)";
	private static final String editProductQuery = "UPDATE product SET productName=?, price=?, quantity=? WHERE productId=?";
	private static final String deleteProductQuery = "DELETE FROM product WHERE productId=?";
	private static final String getIdProductQuery = "SELECT productId FROM product WHERE productName=?";
	private static final String viewAllProductTableQuery = "SELECT * FROM product";
	
	private static ArrayList<Integer> idList = new ArrayList<Integer>();
	private static ArrayList<String> nameList = new ArrayList<String>();
	private static ArrayList<Double> priceList = new ArrayList<Double>();
	private static ArrayList<Integer> quantityList = new ArrayList<Integer>();

	public static Product findProductById(int productId) {
		
		Product product = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement find = null;
		ResultSet rs = null;
		try {
			
			find = dbConnection.prepareStatement(findProductById);
			find.setLong(1, productId);
			rs = find.executeQuery();
			rs.next();
			String productName = rs.getString("productName");
			double price = rs.getDouble("price");
			int quantity = rs.getInt("quantity");
			product = new Product(productId, productName, price, quantity);
		}catch(Exception e) {
			
			e.printStackTrace();
		}finally {
			
			ConnectionFactory.close(find);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(dbConnection);
		}
		
		return product;
	}
	
	public static void insertProduct(Product product) {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insert = null;
		
		try {
			
			insert = dbConnection.prepareStatement(insertProductQuery);
			insert.setLong(1, product.getProductId());
			insert.setString(2, product.getProductName());
			insert.setDouble(3, product.getPrice());
			insert.setLong(4, product.getQuantity());
			insert.executeUpdate();
		}catch(Exception e) {
			
			e.printStackTrace();
		}finally {
			
			ConnectionFactory.close(insert);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static void editProduct(String productName, double price, int quantity, int productId) {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement edit = null;
		
		try {
			
			edit = dbConnection.prepareStatement(editProductQuery);
			edit.setString(1, productName);
			edit.setDouble(2, price);
			edit.setLong(3, quantity);
			edit.setLong(4, productId);
			edit.executeUpdate();
		}catch(Exception e) {
			
			e.printStackTrace();
		}finally {
			
			ConnectionFactory.close(edit);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static void deleteProduct(int productId) {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement delete = null;
		
		try {
			
			delete = dbConnection.prepareStatement(deleteProductQuery);
			delete.setLong(1, productId);
			delete.executeUpdate();
		}catch(Exception e) {
			
			e.printStackTrace();
		}finally {
			
			ConnectionFactory.close(delete);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static void clearList() {
		
		idList.clear();
		nameList.clear();
		priceList.clear();
		quantityList.clear();
	}
	
	public static void viewProductTable() {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement view = null;
		ResultSet rs = null;
		
		try {
			
			view = dbConnection.prepareStatement(viewAllProductTableQuery);
			rs = view.executeQuery();
			clearList();
			while(rs.next()) {
			
				idList.add(rs.getInt("productId"));
				nameList.add(rs.getString("productName"));
				priceList.add(rs.getDouble("price"));
				quantityList.add(rs.getInt("quantity"));
			}
			int i = 0;
			while(i < idList.size()) {
			
				System.out.println("id="+idList.get(i)+", prodName="+nameList.get(i)+", price="+priceList.get(i)+", quantity="+quantityList.get(i));
				i++;
			}
		}catch(Exception e) {
			
			e.printStackTrace();
		}finally {
			
			ConnectionFactory.close(view);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static int getIdProduct(String productName) {
		
		int productId = 0;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement get = null;
		ResultSet rs = null;
		
		try {
			
			get = dbConnection.prepareStatement(getIdProductQuery);
			get.setString(1, productName);
			rs = get.executeQuery();
			rs.next();
			productId = rs.getInt("productId");
		}catch(Exception e) {
			
			e.printStackTrace();
		} finally {
			
			ConnectionFactory.close(get);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(dbConnection);
		}
		return productId;
	}
}
