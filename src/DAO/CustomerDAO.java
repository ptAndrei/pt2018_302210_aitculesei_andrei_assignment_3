package DAO;

import DBAcces.ConnectionFactory;
import Model.Customer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CustomerDAO { //Data access object pattern

	private static final String findCustomerByIdQuery = "SELECT * FROM customer WHERE id=?";
	private static final String insertCustomerQuery = "INSERT INTO customer (name,address,email,age) VALUES (?,?,?,?)";
	private static final String deleteCustomerQuery = "DELETE FROM customer WHERE id=?";
	private static final String editCustomerQuery = "UPDATE customer SET name=?, address=?, email=?, age=? WHERE id=?";
	private static final String getIdCustomerQuery = "SELECT id FROM customer WHERE name=?";
	
	public static Customer findCustomerById(int id) {
		
		Customer customer = null;
		Connection dbConnection = ConnectionFactory.getConnection(); //Connection to DB
		PreparedStatement find = null;
		ResultSet rs = null;
		
		try {
			
			find = dbConnection.prepareStatement(findCustomerByIdQuery); //Initialize the query
			find.setLong(1, id); //Set parameters
			rs = find.executeQuery(); //execute query and store the results in rs
			rs.next();
			
			String name = rs.getString("name");
			String address = rs.getString("address");
			String email = rs.getString("email");
			int age = rs.getInt("age");
			customer = new Customer(id, name, address, email, age);
		}catch (Exception e) {
			
			e.printStackTrace();
		} finally {
			
			ConnectionFactory.close(find);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(dbConnection);
		}
		
		return customer;
	}
	
	public static void insertCustomer(Customer customer) {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insert = null;
		
		try {
			
			insert = dbConnection.prepareStatement(insertCustomerQuery);
			insert.setString(1, customer.getName());
			insert.setString(2, customer.getAddress());
			insert.setString(3, customer.getEmail());
			insert.setInt(4, customer.getAge());
			insert.executeUpdate();
		}catch(Exception e) {
			
			e.printStackTrace();
		}finally {
			
			ConnectionFactory.close(insert);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static void deleteCustomer(int customerId) {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement delete = null;
		
		try {
			
			delete = dbConnection.prepareStatement(deleteCustomerQuery);
			delete.setLong(1, customerId);
			delete.executeUpdate();
		} catch(Exception e) {
			
			e.printStackTrace();
		} finally {
			
			ConnectionFactory.close(delete);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static void editCustomer(String name, String address, String email, int age, int id) {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement edit = null;
		
		try {
			
			edit = dbConnection.prepareStatement(editCustomerQuery);
			edit.setString(1, name);
			edit.setString(2, address);
			edit.setString(3, email);
			edit.setLong(4, age);
			edit.setLong(5, id);
			edit.executeUpdate();
			
		}catch(Exception e) {
			
			e.printStackTrace();
		} finally {
			
			ConnectionFactory.close(edit);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static int getIdCustomer(String name) {
		
		int id = 0;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement get = null;
		ResultSet rs = null;
		
		try {
			
			get = dbConnection.prepareStatement(getIdCustomerQuery);
			get.setString(1, name);
			rs = get.executeQuery();
			rs.next();
			id = rs.getInt("id");
		}catch(Exception e) {
			
			e.printStackTrace();
		} finally {
			
			ConnectionFactory.close(get);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(dbConnection);
		}
		return id;
	}
}
