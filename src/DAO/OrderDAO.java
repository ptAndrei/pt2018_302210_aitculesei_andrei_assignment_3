package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import DBAcces.ConnectionFactory;
import Model.OrderItems;

public class OrderDAO {

	private static final String findOrderByIdQuery = "SELECT * FROM orders WHERE idOrder=?";
	private static final String insertOrderQuery = "INSERT INTO orders (idCustomer,deliveryAddress,productId,quantity) VALUES (?,?,?,?)";
	private static final String editOrderQuery = "UPDATE orders SET idCustomer=?, deliveryAddress=?, productId=?, quantity=? WHERE idOrder=?";
	private static final String deleteOrderQuery = "DELETE FROM orders WHERE idOrder=?";
	
	public static OrderItems findOrderById(int idOrder) {
		
		OrderItems order = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement find = null;
		ResultSet rs = null;
		
		try {
			
			find = dbConnection.prepareStatement(findOrderByIdQuery);
			find.setLong(1, idOrder);
			rs = find.executeQuery();
			rs.next();
			
			int idCustomer = rs.getInt("idCustomer");
			String deliveryAddress = rs.getString("deliveryAddress");
			int productId = rs.getInt("productId");
			int quantity = rs.getInt("quantity");
			order = new OrderItems(idCustomer, deliveryAddress, productId, quantity);
			order.setIdOrder(idOrder);
		}catch(Exception e) {
			
			e.printStackTrace();
		}finally {
			
			ConnectionFactory.close(rs);
			ConnectionFactory.close(find);
			ConnectionFactory.close(dbConnection);
		}
		
		return order;
	}
	
	public static void insertOrder(OrderItems order) {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insert = null;
		
		try {
			
			insert = dbConnection.prepareStatement(insertOrderQuery);
			insert.setLong(1, order.getIdCustomer());
			insert.setString(2, order.getDeliveryAddress());
			insert.setLong(3, order.getProductId());
			insert.setLong(4, order.getQuantity());
			insert.executeUpdate();
		}catch(Exception e) {
			
			e.printStackTrace();
		}finally {
			
			ConnectionFactory.close(insert);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static void deleteOrder(int idOrder) {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement delete = null;
		
		try {
			
			delete = dbConnection.prepareStatement(deleteOrderQuery);
			delete.setLong(1, idOrder);
			delete.executeUpdate();
		}catch(Exception e) {
			
			e.printStackTrace();
		}finally {
			
			ConnectionFactory.close(delete);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public static void editOrder(int idCustomer, String deliveryAddress, int productId, int quantity, int orderId) {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement edit = null;
		
		try {
			
			edit = dbConnection.prepareStatement(editOrderQuery);
			edit.setLong(1, idCustomer);
			edit.setString(2, deliveryAddress);
			edit.setLong(3, productId);
			edit.setLong(4, quantity);
			edit.setLong(5, orderId);
			edit.executeUpdate();
		}catch(Exception e) {
			
			e.printStackTrace();
		}finally {
			
			ConnectionFactory.close(edit);
			ConnectionFactory.close(dbConnection);
		}
	}
}
