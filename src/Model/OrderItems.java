package Model;

public class OrderItems {

	private int idOrder;
	private int idCustomer;
	private String deliveryAddress;
	private int productId;
	private int quantity;
	
	public OrderItems() {
		
	}
	
	public OrderItems(int idCustomer, String deliveryAddress, int productId, int quantity) {
		
		this.idCustomer = idCustomer;
		this.deliveryAddress = deliveryAddress;
		this.productId = productId;
		this.quantity = quantity;
	}
	
	public void setIdOrder(int idOrder) {
		
		this.idOrder = idOrder;
	}
	
	public void setIdCustomer(int idCustomer) {
		
		this.idCustomer = idCustomer;
	}
	
	public void setDeliveryAddress(String deliveryAddress) {
		
		this.deliveryAddress = deliveryAddress;
	}
	
	public void setQuantity(int quantity) {
		
		this.quantity = quantity;
	}
	
	public void setProductId(int productId) {
		
		this.productId = productId;
	}

	public int getIdOrder() {
		
		return idOrder;
	}
	
	public int getIdCustomer() {
		
		return idCustomer;
	}
	
	public String getDeliveryAddress() {
		
		return deliveryAddress;
	}
	
	public int getQuantity() {
		
		return quantity;
	}
	
	public int getProductId() {
		
		return productId;
	}
	
	public String toString() {
		
		return "The cumoster with id="+getIdCustomer()+" placed an order with id="+idOrder+" at address "+getDeliveryAddress()+" of "+quantity+" copies.";
	}
}
