package Model;

public class Product {

	private int productId;
	private String productName;
	private double price;
	private int quantity;
	
	public Product() {
		
	}
	
	public Product(String productName, double price, int quantity) {
		
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}
	
	public Product(int productId, String productName, double price, int quantity) {
		
		this.productId = productId;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}
	
	public void setProductName(String productName) {
		
		this.productName = productName;
	}
	
	public void setProductId(int productId) {
		
		this.productId = productId;
	}
	
	public void setPrice(double price) {
		
		this.price = price;
	}
	
	public void setQuantity(int quantity) {
		
		this.quantity = quantity;
	}
	
	public String getProductName() {
		
		return productName;
	}
	
	public int getProductId() {
		
		return productId;
	}
	
	public double getPrice() {
		
		return price;
	}
	
	public int getQuantity() {
		
		return quantity;
	}
}
