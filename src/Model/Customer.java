package Model;

public class Customer {
	
	private int id;
	private String name;
	private String address;
	private String email;
	private int age;
	
	public Customer() {
		
	}
	
	public Customer(int id, String name, String address, String email, int age) {
		
		this.id = id;
		this.name = name;
		this.address = address;
		this.email = email;
		this.age = age;
	}
	
	public Customer(String name, String address, String email, int age) {
		
		this.name = name;
		this.address = address;
		this.email = email;
		this.age = age;
	}
	
	public void setName(String name) {
		
		this.name = name;
	}
	
	public void setAddress(String address) {
		
		this.address = address;
	}
	
	public void setEmail(String email) {
		
		this.email = email;
	}
	
	public void setAge(int age) {
		
		this.age = age;
	}
	
	public String getName() {
		
		return name;
	}
	
	public int getId() {
		
		return id;
	}
	
	public String getAddress() {
		
		return address;
	}
	
	public String getEmail() {
		
		return email;
	}
	
	public int getAge() {
		
		return age;
	}
	
	public String toString() {
		
		return "Customer with id="+id+", name="+name+", address="+address+", email="+email+" and age="+age;
	}
}
