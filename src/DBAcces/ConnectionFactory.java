package DBAcces;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.DriverManager;


public class ConnectionFactory {
	
	private static ConnectionFactory singleInstance = new ConnectionFactory();
	
	public ConnectionFactory(){

		try {
		
			Class.forName("com.mysql.cj.jdbc.Driver"); //DRIVER initialized through reflexion (insarcineaza implementarea unui driver JDBC) 
		} catch (ClassNotFoundException e) {
			
			System.out.println("Where is your MySQL JDBC Driver?"+e);
		}
		
	}
	
	private Connection createConnection() {
		
		Connection conn = null;
		
		try {
			
			conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/schooldb","root", "root"); //DBURL (db location)
			System.out.println("Connected!");
		}catch(SQLException e) {
			
			e.printStackTrace();
		}
		
		return conn;
	}
	
	public static Connection getConnection() {
		
		return singleInstance.createConnection();
	}
	
	public static void close(Connection conn) {
		
		if (conn != null) {
			
			try {
			
				conn.close();
			}catch(SQLException e) {
			
				/*ignore*/
			}
		}
	}
	
	public static void close(Statement st) {
		
		if (st != null) {

			try {

				st.close();	
			} catch (SQLException e) {

				/*ignore*/
			}
		}
	}
	
	public static void close(ResultSet rs) {
		
		if (rs != null) {
			
			try {

				rs.close();	
			} catch (SQLException e) {
				
				/*ignore*/
			}
		}
	}

}
