package bllValidators;

import Model.Customer;

public class CustomerAgeValidator implements Validator<Customer> {

	private static final int ageMIN = 3;
	private static final int ageMAX = 99;
	
	public void validate(Customer customer) {
		
		if (ageMIN > customer.getAge() || ageMAX < customer.getAge()) {
			
			throw new IllegalArgumentException("The Customer Age limit is not respected!");
		}
	}
}
