package bllValidators;

import java.util.regex.Pattern;
import Model.Customer;

public class CustomerNameValidator implements Validator<Customer> {

	public static final String NAME_PATTERN = "[a-zA-Z]+";

	public void validate(Customer customer) {
		
		if (!Pattern.matches(NAME_PATTERN, customer.getName())) { // -.matches() returneaza true/false
			
			throw new IllegalArgumentException("Is not a valid name!");
		}
	}
}
