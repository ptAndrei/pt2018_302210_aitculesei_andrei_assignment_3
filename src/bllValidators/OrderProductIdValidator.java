package bllValidators;

import java.util.EmptyStackException;
import java.util.NoSuchElementException;

import javax.swing.JOptionPane;

import BLL.ProductBLL;
import DAO.ProductDAO;
import Model.OrderItems;
import Model.Product;

public class OrderProductIdValidator implements Validator<OrderItems> {

	public void validate(OrderItems t) {
		
		Product product = ProductDAO.findProductById(t.getProductId());
		
		if (product == null) {
			
			throw new NoSuchElementException("Product with id="+t.getProductId()+" was not found!");
		}else {
			
			if (t.getQuantity() <= product.getQuantity()) {
				
				ProductBLL products = new ProductBLL();
				product.setQuantity(product.getQuantity() - t.getQuantity());
				products.editProduct(product.getProductName(), product.getPrice(), product.getQuantity(), product.getProductId());
			}else {
				
				JOptionPane.showMessageDialog(null, "Under stock!");
				throw new EmptyStackException();
			}
		}
	}

	
}
