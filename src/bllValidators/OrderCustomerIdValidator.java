package bllValidators;

import java.util.NoSuchElementException;

import DAO.CustomerDAO;
import Model.Customer;
import Model.OrderItems;

public class OrderCustomerIdValidator implements Validator<OrderItems> {

	public void validate(OrderItems t) {
		
		Customer customer = CustomerDAO.findCustomerById(t.getIdCustomer());
		
		if (customer == null) {

			throw new NoSuchElementException("Customer with id="+t.getIdCustomer()+" was not found!");
		}
	}
}
