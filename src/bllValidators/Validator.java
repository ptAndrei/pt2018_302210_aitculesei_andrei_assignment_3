package bllValidators;

public interface Validator<T> {

	public void validate(T t);
}
