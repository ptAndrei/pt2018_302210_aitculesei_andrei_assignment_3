package Reflection;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class Reflexion {
	
	private static ArrayList<String> listaNumeColoane = new ArrayList<String>();
	private static ArrayList<String> tableRowsList;
	private static int nrCustomerTableRows = 0;
	
	public static void reflexion(Object object) {
		
		tableRowsList = new ArrayList<String>();
		
		for (Field field : object.getClass().getDeclaredFields()) {

			field.setAccessible(true);
			listaNumeColoane.add(field.getName());
			Object value;
			try {
				value = field.get(object);
				tableRowsList.add(value+"");

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		nrCustomerTableRows++;
	}
	
	public static void resetNrCustomerTableRows() {
		
		nrCustomerTableRows = 0;
	}
	
	public static void clearListaNumeColoane() {
		
		listaNumeColoane.clear();
	}
	
	public static void clearTableRowsList() {
		
		tableRowsList.clear();
	}
	
	public static ArrayList<String> getListaNumeDeColoane(){
		
		return listaNumeColoane;
	}
	
	public static ArrayList<String> getTableRowsList(){
		
		return tableRowsList;
	}
	
	public static int getNrCustomerTableRows() {
		
		return nrCustomerTableRows;
	}
}
