package BLL;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import DAO.OrderDAO;
import Model.OrderItems;
import bllValidators.Validator;
import bllValidators.OrderCustomerIdValidator;
import bllValidators.OrderProductIdValidator;

public class OrderBLL {
	
	private ArrayList<Validator<OrderItems>> validators;
	
	public OrderBLL(){
		
		validators = new ArrayList<Validator<OrderItems>>();
		validators.add(new OrderCustomerIdValidator());
		validators.add(new OrderProductIdValidator());
	}

	public OrderItems findOrderById(int id) {
		
		OrderItems order = OrderDAO.findOrderById(id);
		
		if (order == null) {
			
			throw new NoSuchElementException("Order with id="+id+" was not found!");
		}
		return order;
	}
	
	public void insertOrder(OrderItems order) {
		
		for(Validator<OrderItems> v : validators) {
			
			v.validate(order);
		}
		OrderDAO.insertOrder(order);
	}
	
	public void deleteOrder(int id) {
		
		OrderItems order = OrderDAO.findOrderById(id);
		
		if (order != null) {
			
			OrderDAO.deleteOrder(id);
		}
		else {
			
			throw new NoSuchElementException("Order with id="+id+" was not found!");
		}
	}
	
	public void editOrder(int idCustomer, String deliveryAddress, int productId, int quantity, int id) {
		
		OrderItems order = OrderDAO.findOrderById(id);
		
		if (order != null) {
			
			order = new OrderItems(idCustomer, deliveryAddress, productId, quantity);
			for(Validator<OrderItems> v : validators) {
				
				v.validate(order);
			}
			OrderDAO.editOrder(idCustomer, deliveryAddress, productId, quantity, id);
		}
		else {
			
			throw new NoSuchElementException("Order with id="+id+" was not found!");
		}
	}
}
