package BLL;

import java.util.NoSuchElementException;

import DAO.ProductDAO;
import Model.Product;

public class ProductBLL {

	public Product findProductById(int id) {
		
		Product product = ProductDAO.findProductById(id);
		
		if (product == null) {
			
			throw new NoSuchElementException("Product with id="+id+" was not found!");
		}
		return product;
	}
	
	public void insertProduct(Product product) {
		
		ProductDAO.insertProduct(product);
	}
	
	public void deleteProduct(int id) {
		
		Product product = ProductDAO.findProductById(id);
		
		if (product != null) {
			
			ProductDAO.deleteProduct(id);
		}else {
			
			throw new NoSuchElementException("Product with id="+id+" was not found!");
		}
	}
	
	public void editProduct(String productName, double price, int quantity, int id) {
		
		Product product = ProductDAO.findProductById(id);
		
		if (product != null) {
			
			ProductDAO.editProduct(productName, price, quantity, id);
		}else {
			
			throw new NoSuchElementException("Product with id="+id+" was not found!");
		}
	}
}
