package BLL;

import Model.Customer;
import bllValidators.*;
import DAO.CustomerDAO;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class CustomerBLL {

	private ArrayList<Validator<Customer>> validators;
	
	public CustomerBLL() {
		
		validators = new ArrayList<Validator<Customer>>();
		validators.add(new EmailValidator());
		validators.add(new CustomerAgeValidator());
		validators.add(new CustomerNameValidator());
	}

	public Customer findCustomerById(int id) {
		
		Customer customer = CustomerDAO.findCustomerById(id);
		
		if (customer == null) {
			
			throw new NoSuchElementException("The customer with id =" + id + " was not found!");
		}
		return customer;
	}

	public void insertCustomer(Customer customer) {
		
		for (Validator<Customer> v : validators) {
			
			v.validate(customer);
		}
		CustomerDAO.insertCustomer(customer);
	}

	public void deleteCustomer(int id) {
		
		Customer customer = CustomerDAO.findCustomerById(id);
		
		if (customer != null) {
			
			CustomerDAO.deleteCustomer(id);
		}
		else {
			
			throw new NoSuchElementException("Customer with id="+id+" was not found!");
		}
	}
	
	public void editCustomer(String name, String address, String email, int age, int id) {
		
		Customer customer = CustomerDAO.findCustomerById(id);

		if (customer != null) {
			
			customer = new Customer(name, address, email, age);
			for (Validator<Customer> v : validators) {
				
				v.validate(customer);
			}
			CustomerDAO.editCustomer(name, address, email, age, id);
		}
		else {
			
			throw new NoSuchElementException("Customer with id="+id+" was not found!");
		}
	}
}
