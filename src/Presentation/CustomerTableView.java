package Presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import DBAcces.ConnectionFactory;
import Model.Customer;
import Reflection.Reflexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CustomerTableView {

	public DefaultTableModel customersTableModel = new DefaultTableModel();
	public JTable customersTable;
	
	private JFrame customerTableView  = new JFrame("Customers");
	private JPanel tablePanel = new JPanel();
	private JPanel optionsPanel = new JPanel();
	
	public JLabel idLabel = new JLabel("Customer ID:");
	public JLabel nameLabel = new JLabel("Name:");
	public JLabel addressLabel = new JLabel("Address:");
	public JLabel emailLabel = new JLabel("Email:");
	public JLabel ageLabel = new JLabel("Age:");
	
	public JTextField textIdCustomer = new JTextField();
	public JTextField textCustomerName = new JTextField();
	public JTextField textAddress = new JTextField();
	public JTextField textEmail = new JTextField();
	public JTextField textAge = new JTextField();
	
	public JButton btnAdd = new JButton("Add");
	public JButton btnDelete = new JButton("Delete");
	public JButton btnUpdate = new JButton("Update");
	public JButton exitBtn = new JButton("Exit");
	
	public CustomerTableView() {

		//Create table
		customersTable = new JTable(customersTableModel);
		Customer customer = new Customer();
		Reflexion.reflexion(customer);
		for(String coloana : Reflexion.getListaNumeDeColoane())
			customersTableModel.addColumn(coloana);
		Reflexion.clearListaNumeColoane();
		tablePanel.setLayout(new BorderLayout());
		tablePanel.add(customersTable, BorderLayout.CENTER);
		tablePanel.add(new JScrollPane(customersTable));
		fillTable();
		//Create frame
		customerTableView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		customerTableView.setLayout(new GridLayout(2,1));
		customerTableView.setSize(new Dimension(700, 700));
		customerTableView.setLocationRelativeTo(null);

		customerTableView.add(tablePanel);
		customerTableView.add(optionsPanel);
		
		///////
		optionsPanel.setLayout(new GridBagLayout());
		GridBagConstraints customerConstr = new GridBagConstraints();
		customerConstr.fill = GridBagConstraints.HORIZONTAL;
		
		customerConstr.gridx = 0;
		customerConstr.gridy = 0;
		optionsPanel.add(idLabel, customerConstr);
		customerConstr.gridx = 1;
		customerConstr.gridy = 0;
		optionsPanel.add(textIdCustomer, customerConstr);
		customerConstr.gridx = 0;
		customerConstr.gridy = 1;
		optionsPanel.add(nameLabel, customerConstr);
		customerConstr.gridx = 1;
		customerConstr.gridy = 1;
		optionsPanel.add(textCustomerName, customerConstr);
		customerConstr.gridx = 0;
		customerConstr.gridy = 2;
		optionsPanel.add(addressLabel, customerConstr);
		customerConstr.gridx = 1;
		customerConstr.gridy = 2;
		optionsPanel.add(textAddress, customerConstr);
		customerConstr.gridx = 0;
		customerConstr.gridy = 3;
		optionsPanel.add(emailLabel, customerConstr);
		customerConstr.gridx = 1;
		customerConstr.gridy = 3;
		optionsPanel.add(textEmail, customerConstr);
		customerConstr.gridx = 0;
		customerConstr.gridy = 4;
		optionsPanel.add(ageLabel, customerConstr);
		customerConstr.gridx = 1;
		customerConstr.gridy = 4;
		optionsPanel.add(textAge, customerConstr);
		customerConstr.gridx = 1;
		customerConstr.gridy = 5;
		optionsPanel.add(btnAdd, customerConstr);
		customerConstr.gridx = 1;
		customerConstr.gridy = 6;
		optionsPanel.add(btnUpdate, customerConstr);
		customerConstr.gridx = 1;
		customerConstr.gridy = 7;
		optionsPanel.add(btnDelete, customerConstr);
		customerConstr.gridx = 1;
		customerConstr.gridy = 8;
		optionsPanel.add(exitBtn, customerConstr);
		
	}
	
	public void addRows(int id) {
		
		customersTableModel.addRow(new Object[] {id, Reflexion.getTableRowsList().get(1), Reflexion.getTableRowsList().get(2), Reflexion.getTableRowsList().get(3), Reflexion.getTableRowsList().get(4)});
	}

	public void fillTable() {
		
		for (int i = customersTableModel.getRowCount() - 1; i >= 0; i--) {
			
			customersTableModel.removeRow(i);
		}
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement fill = null;
		ResultSet rs = null;
		
		try {
			
			fill = dbConnection.prepareStatement("SELECT * FROM customer");
			rs = fill.executeQuery();
			while(rs.next()) {
				
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				String email = rs.getString("email");
				int age = rs.getInt("age");
				customersTableModel.addRow(new Object[] {id, name, address, email, age});
			}
		}catch(Exception e) {
			
			e.printStackTrace();
		} finally {
			
			ConnectionFactory.close(fill);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public void setVisible(boolean b) {
		
		customerTableView.setVisible(b);
	}
}
