package Presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import DBAcces.ConnectionFactory;
import Model.OrderItems;
import Reflection.Reflexion;

public class OrderTableView {

	public DefaultTableModel ordersTableModel = new DefaultTableModel();
	public JTable ordersTable;

	private JFrame orderTableView  = new JFrame("Orders");
	private JPanel tablePanel = new JPanel();
	private JPanel optionsPanel = new JPanel();

	public JLabel orderIdLabel = new JLabel("Order ID:");
	public JLabel customerIdLabel = new JLabel("Customer ID:");
	public JLabel deliveryAddressLabel = new JLabel("Delivery address:");
	public JLabel productIdLabel = new JLabel("ID product:");
	public JLabel quantityLabel = new JLabel("Quantity:");

	public JTextField textOrderId = new JTextField();
	public JTextField textCustomerId = new JTextField();
	public JTextField textDeliveryAddress = new JTextField();
	public JTextField textProductId = new JTextField();
	public JTextField textQuantity = new JTextField();

	public JButton btnAdd = new JButton("Add");
	public JButton btnDelete = new JButton("Delete");
	public JButton btnUpdate = new JButton("Update");
	public JButton exitBtn = new JButton("Exit");

	public OrderTableView() {
		
		//Create table
		ordersTable = new JTable(ordersTableModel);
		OrderItems order = new OrderItems();
		Reflexion.reflexion(order);
		for(String coloana : Reflexion.getListaNumeDeColoane())
			ordersTableModel.addColumn(coloana);
		Reflexion.clearListaNumeColoane();
		tablePanel.setLayout(new BorderLayout());
		tablePanel.add(ordersTable, BorderLayout.CENTER);
		tablePanel.add(new JScrollPane(ordersTable));
		fillTable();
		//Create frame
		orderTableView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		orderTableView.setLayout(new GridLayout(2,1));
		orderTableView.setSize(new Dimension(700, 700));
		orderTableView.setLocationRelativeTo(null);

		orderTableView.add(tablePanel);
		orderTableView.add(optionsPanel);

		///////
		optionsPanel.setLayout(new GridBagLayout());
		GridBagConstraints orderConstr = new GridBagConstraints();
		orderConstr.fill = GridBagConstraints.HORIZONTAL;

		orderConstr.gridx = 0;
		orderConstr.gridy = 0;
		optionsPanel.add(orderIdLabel, orderConstr);
		orderConstr.gridx = 1;
		orderConstr.gridy = 0;
		optionsPanel.add(textOrderId, orderConstr);
		orderConstr.gridx = 0;
		orderConstr.gridy = 1;
		optionsPanel.add(customerIdLabel, orderConstr);
		orderConstr.gridx = 1;
		orderConstr.gridy = 1;
		optionsPanel.add(textCustomerId, orderConstr);
		orderConstr.gridx = 0;
		orderConstr.gridy = 2;
		optionsPanel.add(deliveryAddressLabel, orderConstr);
		orderConstr.gridx = 1;
		orderConstr.gridy = 2;
		optionsPanel.add(textDeliveryAddress, orderConstr);
		orderConstr.gridx = 0;
		orderConstr.gridy = 3;
		optionsPanel.add(productIdLabel, orderConstr);
		orderConstr.gridx = 1;
		orderConstr.gridy = 3;
		optionsPanel.add(textProductId, orderConstr);
		orderConstr.gridx = 0;
		orderConstr.gridy = 4;
		optionsPanel.add(quantityLabel, orderConstr);
		orderConstr.gridx = 1;
		orderConstr.gridy = 4;
		optionsPanel.add(textQuantity, orderConstr);
		orderConstr.gridx = 1;
		orderConstr.gridy = 5;
		optionsPanel.add(btnAdd, orderConstr);
		orderConstr.gridx = 1;
		orderConstr.gridy = 6;
		optionsPanel.add(btnUpdate, orderConstr);
		orderConstr.gridx = 1;
		orderConstr.gridy = 7;
		optionsPanel.add(btnDelete, orderConstr);
		orderConstr.gridx = 1;
		orderConstr.gridy = 8;
		optionsPanel.add(exitBtn, orderConstr);

	}
	
	public void addRows() {
		
		ordersTableModel.addRow(new Object[] {0, Reflexion.getTableRowsList().get(1), Reflexion.getTableRowsList().get(2), Reflexion.getTableRowsList().get(3), Reflexion.getTableRowsList().get(4)});
	}
	
	public void fillTable() {
		
		for (int i = ordersTableModel.getRowCount() - 1; i >= 0; i--) {
			
			ordersTableModel.removeRow(i);
		}
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement fill = null;
		ResultSet rs = null;
		
		try {
			
			fill = dbConnection.prepareStatement("SELECT * FROM orders");
			rs = fill.executeQuery();
			while(rs.next()) {
				
				int idOrder = rs.getInt("idorder");
				int idCustomer = rs.getInt("idCustomer");
				String address = rs.getString("deliveryAddress");
				int idProduct = rs.getInt("productId");
				int quantity = rs.getInt("quantity");
				ordersTableModel.addRow(new Object[] {idOrder, idCustomer, address, idProduct, quantity});
			}
		}catch(Exception e) {
			
			e.printStackTrace();
		} finally {
			
			ConnectionFactory.close(fill);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(dbConnection);
		}
	}

	public void setVisible(boolean b) {

		orderTableView.setVisible(b);
	}
}
