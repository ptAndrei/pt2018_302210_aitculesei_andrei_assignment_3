package Presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class View {
	
	private JFrame fereastraPrincipala;
	public JPanel mainPanel = new JPanel();
	public JPanel exitPanel = new JPanel();
	
	public JButton customerTableBtn = new JButton("Customers");
	public JButton orderTableBtn = new JButton("Orders");
	public JButton productTableBtn = new JButton("Products");
	public JButton exitBtn = new JButton("Exit");
	
	public View() {
		
		fereastraPrincipala = new JFrame("Warehouse Management");
		fereastraPrincipala.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fereastraPrincipala.setLayout(new BorderLayout());
		fereastraPrincipala.setSize(new Dimension(500, 150));
		mainPanel.setLayout(new BorderLayout());
		
		customerTableBtn.setBounds(150,220,100,25);
		
		mainPanel.add(customerTableBtn, BorderLayout.NORTH);
		mainPanel.add(orderTableBtn, BorderLayout.CENTER);
		mainPanel.add(productTableBtn, BorderLayout.SOUTH);
		exitPanel.add(exitBtn, BorderLayout.SOUTH);
		
		fereastraPrincipala.add(mainPanel, BorderLayout.CENTER);
		fereastraPrincipala.add(exitPanel, BorderLayout.SOUTH);
		
		fereastraPrincipala.setVisible(true);
	}
}
