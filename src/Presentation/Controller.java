package Presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import BLL.*;
import DAO.CustomerDAO;
import DAO.ProductDAO;
import Model.*;
import Reflection.Reflexion;

public class Controller {

	private View view;
	private CustomerTableView customersView;
	private OrderTableView ordersView;
	private ProductTableView productsView;
	private CustomerBLL customers = new CustomerBLL();
	private OrderBLL orders = new OrderBLL();
	private ProductBLL products = new ProductBLL();
	
	public Controller(View view) {
		
		this.view = view;
		control();
	}
	
	public void control() {
		
		///Customers table
		view.customerTableBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				customersView = new CustomerTableView();
				customersView.setVisible(true);
				
				customersView.btnAdd.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						Customer customer;
						String name = customersView.textCustomerName.getText();
						String address = customersView.textAddress.getText();
						String email = customersView.textEmail.getText();
						int age = Integer.parseInt(customersView.textAge.getText());
						customer = new Customer(name, address, email, age);
						customers.insertCustomer(customer);
						int id = CustomerDAO.getIdCustomer(customer.getName());
						Reflexion.reflexion(customer);
						customersView.addRows(id);
					}
				});
				
				customersView.btnDelete.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						int idCustomer = Integer.parseInt(customersView.textIdCustomer.getText());
						customers.deleteCustomer(idCustomer);
						customersView.fillTable();
					}
				});
				
				customersView.btnUpdate.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						String name = customersView.textCustomerName.getText();
						String address = customersView.textAddress.getText();
						String email = customersView.textEmail.getText();
						int age = Integer.parseInt(customersView.textAge.getText());
						int id = Integer.parseInt(customersView.textIdCustomer.getText());
						customers.editCustomer(name, address, email, age, id);
						customersView.fillTable();
					}
				});
				
				customersView.exitBtn.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						customersView.setVisible(false);
					}
				});
			}
		});
		
		///Orders table
		view.orderTableBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ordersView = new OrderTableView();
				ordersView.setVisible(true);
				
				ordersView.btnAdd.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						OrderItems order;
						int customerId = Integer.parseInt(ordersView.textCustomerId.getText());
						String deliveryAddress = ordersView.textDeliveryAddress.getText();
						int productId = Integer.parseInt(ordersView.textProductId.getText());
						int quantity = Integer.parseInt(ordersView.textQuantity.getText());
						order = new OrderItems(customerId, deliveryAddress, productId, quantity);
						orders.insertOrder(order);
						Reflexion.reflexion(order);
						ordersView.addRows();
						ordersView.fillTable();
					}
				});
				
				ordersView.btnDelete.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						int orderId = Integer.parseInt(ordersView.textOrderId.getText());
						orders.deleteOrder(orderId);
						ordersView.fillTable();
					}
				});
				
				ordersView.btnUpdate.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						int idCustomer = Integer.parseInt(ordersView.textCustomerId.getText());
						String deliveryAddress = ordersView.textDeliveryAddress.getText();
						int productId = Integer.parseInt(ordersView.textProductId.getText());
						int quantity = Integer.parseInt(ordersView.textQuantity.getText());
						int idOrder = Integer.parseInt(ordersView.textOrderId.getText());
						orders.editOrder(idCustomer, deliveryAddress, productId, quantity, idOrder);
						ordersView.fillTable();
					}
				});
				
				ordersView.exitBtn.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						ordersView.setVisible(false);
					}
				});
			}
		});
		
		///Products table
		view.productTableBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				productsView = new ProductTableView();
				productsView.setVisible(true);
				
				productsView.btnAdd.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						Product product;
						String productName = productsView.textProductName.getText();
						double price = Double.parseDouble(productsView.textPrice.getText());
						int quantity = Integer.parseInt(productsView.textQuantity.getText());
						product = new Product(productName, price, quantity);
						products.insertProduct(product);
						int id = ProductDAO.getIdProduct(product.getProductName());
						Reflexion.reflexion(product);
						productsView.addRows(id);
					}
				});
				
				productsView.btnDelete.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						int productId = Integer.parseInt(productsView.textProductId.getText());
						products.deleteProduct(productId);
						productsView.fillTable();
					}
				});
				
				productsView.btnUpdate.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						String productName = productsView.textProductName.getText();
						double price = Double.parseDouble(productsView.textPrice.getText());
						int quantity = Integer.parseInt(productsView.textQuantity.getText());
						int productId = Integer.parseInt(productsView.textProductId.getText());
						products.editProduct(productName, price, quantity, productId);
						productsView.fillTable();
					}
				});
				
				productsView.refreshTableBtn.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						productsView.fillTable();
					}
				});
				
				productsView.exitBtn.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						productsView.setVisible(false);
					}
				});
			}
		});
		
		view.exitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			}
		});
	}
}
