package Presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import DBAcces.ConnectionFactory;
import Model.Product;
import Reflection.Reflexion;

public class ProductTableView {

	public DefaultTableModel productsTableModel = new DefaultTableModel();
	public JTable productsTable;
	
	private JFrame productTableView  = new JFrame("Products");
	private JPanel tablePanel = new JPanel();
	private JPanel optionsPanel = new JPanel();
	
	public JLabel productIdLabel = new JLabel("Product ID:");
	public JLabel productNameLabel = new JLabel("Product name:");
	public JLabel quantityLabel = new JLabel("Quantity:");
	public JLabel priceLabel = new JLabel("Price:");
	
	public JTextField textProductId = new JTextField();
	public JTextField textProductName = new JTextField();
	public JTextField textQuantity = new JTextField();
	public JTextField textPrice = new JTextField();
	
	public JButton btnAdd = new JButton("Add");
	public JButton btnDelete = new JButton("Delete");
	public JButton btnUpdate = new JButton("Update");
	public JButton refreshTableBtn = new JButton("Refresh");
	public JButton exitBtn = new JButton("Exit");
	
	public ProductTableView() {

		//Create table
		productsTable = new JTable(productsTableModel);
		Product customer = new Product();
		Reflexion.reflexion(customer);
		for(String coloana : Reflexion.getListaNumeDeColoane())
			productsTableModel.addColumn(coloana);
		Reflexion.clearListaNumeColoane();
		tablePanel.setLayout(new BorderLayout());
		tablePanel.add(productsTable, BorderLayout.CENTER);
		tablePanel.add(new JScrollPane(productsTable));
		fillTable();
		//Create frame
		productTableView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		productTableView.setLayout(new GridLayout(2,1));
		productTableView.setSize(new Dimension(700, 700));
		productTableView.setLocationRelativeTo(null);

		productTableView.add(tablePanel);
		productTableView.add(optionsPanel);
		
		///////
		optionsPanel.setLayout(new GridBagLayout());
		GridBagConstraints productConstr = new GridBagConstraints();
		productConstr.fill = GridBagConstraints.HORIZONTAL;
		
		productConstr.gridx = 0;
		productConstr.gridy = 0;
		optionsPanel.add(productIdLabel, productConstr);
		productConstr.gridx = 1;
		productConstr.gridy = 0;
		optionsPanel.add(textProductId, productConstr);
		productConstr.gridx = 0;
		productConstr.gridy = 1;
		optionsPanel.add(productNameLabel, productConstr);
		productConstr.gridx = 1;
		productConstr.gridy = 1;
		optionsPanel.add(textProductName, productConstr);
		productConstr.gridx = 0;
		productConstr.gridy = 2;
		optionsPanel.add(quantityLabel, productConstr);
		productConstr.gridx = 1;
		productConstr.gridy = 2;
		optionsPanel.add(textQuantity, productConstr);
		productConstr.gridx = 0;
		productConstr.gridy = 3;
		optionsPanel.add(priceLabel, productConstr);
		productConstr.gridx = 1;
		productConstr.gridy = 3;
		optionsPanel.add(textPrice, productConstr);
		productConstr.gridx = 1;
		productConstr.gridy = 4;
		optionsPanel.add(btnAdd, productConstr);
		productConstr.gridx = 1;
		productConstr.gridy = 5;
		optionsPanel.add(btnUpdate, productConstr);
		productConstr.gridx = 1;
		productConstr.gridy = 6;
		optionsPanel.add(btnDelete, productConstr);
		productConstr.gridx = 0;
		productConstr.gridy = 7;
		optionsPanel.add(refreshTableBtn, productConstr);
		productConstr.gridx = 1;
		productConstr.gridy = 7;
		optionsPanel.add(exitBtn, productConstr);
		
	}
	
	public void addRows(int id) {
		
		productsTableModel.addRow(new Object[] {id, Reflexion.getTableRowsList().get(1), Reflexion.getTableRowsList().get(2), Reflexion.getTableRowsList().get(3)});
	}
	
	public void fillTable() {
		
		for (int i = productsTableModel.getRowCount() - 1; i >= 0; i--) {
			
			productsTableModel.removeRow(i);
		}
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement fill = null;
		ResultSet rs = null;
		
		try {
			
			fill = dbConnection.prepareStatement("SELECT * FROM product");
			rs = fill.executeQuery();
			while(rs.next()) {
				
				int id = rs.getInt("productId");
				String productName = rs.getString("productName");
				double price = rs.getDouble("price");
				int quantity = rs.getInt("quantity");
				productsTableModel.addRow(new Object[] {id, productName, price, quantity});
			}
		}catch(Exception e) {
			
			e.printStackTrace();
		}
	}

	public void setVisible(boolean b) {
		
		productTableView.setVisible(b);
	}
}
